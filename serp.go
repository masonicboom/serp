// serp: command-line recursive search-and-replace utility with confirmation.
// LICENSE: Public domain.

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
        "github.com/daviddengcn/go-colortext"
)

var rootDir string
var searchRegex string
var replace string
var filenameMatcher string
var recurse bool
var confirm bool

func init() {
	flag.StringVar(&rootDir, "root", ".", "Directory to begin descent in.")
	flag.StringVar(&searchRegex, "search", "", "Regular expression to search for.")
	flag.StringVar(&replace, "replace", "", "Text to replace matches with.")
	flag.StringVar(&filenameMatcher, "pattern", "*", "Pattern to match files against before processing.")
	flag.BoolVar(&recurse, "recurse", true, "Whether to recursively descend into directories.")
	flag.BoolVar(&confirm, "confirm", true, "Whether to confirm replacements.")
}

func confirmReplacement(filePath string, regex *regexp.Regexp, replacement []byte) func([]byte) []byte {
	return func(candidate []byte) []byte {
		match := regex.FindSubmatchIndex(candidate)
		var dest []byte
		expanded := regex.Expand(dest, replacement, candidate, match)

                originalColor := ct.Blue
                candidateColor := ct.Yellow

                ct.ChangeColor(ct.Green, false, ct.None, false)
		fmt.Printf("%s:\t", filePath)
                ct.ResetColor()
                fmt.Printf("replace ")
                ct.ChangeColor(originalColor, false, ct.None, false)
                fmt.Printf("%s", candidate)
                ct.ResetColor()
                fmt.Printf(" with ")
                ct.ChangeColor(candidateColor, false, ct.None, false)
                fmt.Printf("%s", expanded)
                ct.ResetColor()
                fmt.Printf("? (")
                ct.ChangeColor(candidateColor, false, ct.None, false)
                fmt.Printf("y")
                ct.ResetColor()
                fmt.Printf("/[")
                ct.ChangeColor(originalColor, false, ct.None, false)
                fmt.Printf("n")
                ct.ResetColor()
                fmt.Printf("]) ")

		var response string
		_, err := fmt.Scanln(&response)
		if err != nil && err.Error() != "unexpected newline" {
			log.Fatal(err)
		}
		if response == "y" {
			return expanded
		} else {
			return candidate
		}
	}
}

func searchAndReplaceFile(filePath string, regex *regexp.Regexp) {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatal("Failed to open file: ", filePath)
	}

	repl := []byte(replace)
	replacer := confirmReplacement(filePath, regex, repl)
        var replaced []byte
	if confirm {
		replaced = regex.ReplaceAllFunc(bytes, replacer)
	} else {
		replaced = regex.ReplaceAll(bytes, repl)
	}

	ioutil.WriteFile(filePath, replaced, os.ModeTemporary)
}

func searchDir(dirPath string, regex *regexp.Regexp) {
	dir, err := os.Open(dirPath)
	if err != nil {
		log.Fatal("Failed to open given root dir: ", dirPath)
	}

	fileNames, err := dir.Readdir(0)
	if err != nil {
		log.Fatal("Failed to list files in ", dirPath)
	}

	for _, f := range fileNames {
		filename := f.Name()
		fullPath := path.Join(dirPath, filename)

		if f.IsDir() {
			if recurse {
				searchDir(fullPath, regex)
			}
		} else {
			matched, err := filepath.Match(filenameMatcher, filename)
			if err != nil {
				log.Fatal("Failed trying to match %s against pattern %s.", filename, filenameMatcher)
			}
			if matched {
				searchAndReplaceFile(fullPath, regex)
			}
		}
	}
}

func main() {
	flag.Parse()

	re, err := regexp.Compile(searchRegex)
	if err != nil {
		log.Fatal("Failed to compile regular expression: ", searchRegex)
	}

	searchDir(rootDir, re)
}
