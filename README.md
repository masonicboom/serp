# Dependencies

    go get github.com/daviddengcn/go-colortext

# Build

    go build

# Example

    ./serp --root test --search hi --replace bye --pattern "*"

Then examine test/z.

# TODO

Automated testing.

# LICENSE

Public domain.
